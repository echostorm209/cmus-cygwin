# cmus-cygwin

This is a configure script with a very minor fix that allows Cmus to be compiled under Cygwin64. 
Just replace the configure script included with the Cmus source code with this one, then go through the usual "configure, make, make install" routine.
